global _start

%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 256
%define NEW_STRING_SYMBOL 10

extern find_word


section .rodata
overflow_mess: db NEW_STRING_SYMBOL, "Превышен размер буффера <=255", NEW_STRING_SYMBOL, 0
not_found_mess: db "Нет значения по такому ключу", NEW_STRING_SYMBOL, 0

section .bss
buffer: resb BUFFER_SIZE

section .text

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
.check_overflow:
	test rax, rax
	je .out_bounds_size

.check_word:
	mov rdi, rax
	mov rsi, next_element
	push rdx
	call find_word
	pop rdx
.isHave_key:
	test rax, rax
	je .not_found_key

.continue:
	mov rdi, rax
	add rdi, 9
	add rdi, rdx
	call print_string

.out_bounds_size:
	mov rdi, overflow_mess 
	call error 
	jmp .end

.not_found_key:
	mov rdi, not_found_mess 
	call error
	jmp .end

.end:
	call print_newline 
	xor rdi, rdi
	jmp exit 
