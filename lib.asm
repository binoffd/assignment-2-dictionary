section .text

%define EXIT 60
%define STDOUT 1 
%define NEW_STRING 0xA
%define STDERR 2
%define NULL_TERMINATOR 0
%define SPACE 0x20
%define TAB 0x9
%define MIN 0x30
%define MAX 0x39
%define SYS_CALL 1

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_message
global error
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    push rbx            
 .lp:
    mov bl, [rdi+rax]
    test bl, bl
    jz .return
    inc rax
    jmp .lp
 .return:
    pop rbx             
    ret

; rdi принимает указатель, rsi принимает сообщение об ошибке
print_message:
	call string_length
	mov rdx, rax
	mov rsi, rdi
	mov rdi, rcx
	mov rax, STDOUT
	syscall
	ret

error:
	mov rcx, STDERR
	jmp print_message

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, STDOUT
    mov rax, SYS_CALL
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ;
    
    mov rax, SYS_CALL ;
    mov rdi, STDOUT ;
    mov rsi, rsp ; 
    mov rdx, 1
    syscall
    pop rdi

    xor rax, rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_STRING
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov rsi, 1
    push rbx
    dec rsp
    mov rbx, 0xA

.loop:
    xor rdx, rdx
    div rbx
    add rdx, 0x30  
    dec rsp
    mov [rsp], dl
    inc rsi
    test rax, rax
    jnz .loop

.print_d:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .positive
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    
.positive:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax

.loop:
    mov bl, [rdi+rax]
    cmp bl, byte[rsi+rax]
    jne .noequals
    test bl, bl
    je .equals
    inc rax
    jmp .loop

.noequals:
    xor rax, rax
    ret

.equals:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push byte 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
 .loop:
	push rcx
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	pop rcx
	cmp rax, SPACE
	je .space_symbol
	cmp rax, TAB
	je .space_symbol
	cmp rax, NEW_STRING
	je .space_symbol
	test rax, rax
	jz .finish
	cmp rsi, rcx
	je .overflow
	mov byte[rdi+rcx], al
	inc rcx
	jmp .loop
 .overflow:
	xor rax, rax
	ret
 .space_symbol:
	test rcx, rcx
	je .loop
 .finish:
	mov byte[rdi+rcx], NULL_TERMINATOR
	mov rax, rdi
	mov rdx, rcx
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, 10;

.loop:
    push rsi
    mov sil, [rdi]
    test sil, sil
    je .stop
    cmp sil, NEW_STRING
    je .stop
    cmp sil, MIN
    jb .stop
    cmp sil, MAX
    ja .stop
    sub sil, MIN
    mul r8
    add rax, rsi
    inc rdi
    pop rsi
    inc rsi
    jmp .loop

.stop:
    pop rdx
    ret    

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi;
    call parse_uint
    pop rdi
    test rdx, rdx
    jne .stop
    mov sil, [rdi]
    inc rdi
    push rsi
    call parse_uint
    inc rdx
    pop rsi
    cmp sil, '-'
    jne .stop
    neg rax
    jne .stop

.stop:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0	
string_copy:
    xor rax, rax
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .end
.loop:
    mov dl, [rdi+r8]
    mov [rsi+r8], dl
    inc r8
    cmp r8, rax
    jle .loop
.end:
    xor rax, rax
    ret
